package mvc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import usedClass.ItemChangeListener;

public class OngletDetailVue {
	
	private OngletDetailControleur controleur;
	
	private DeviceDataViewerVue vue;
	
	private JPanel globale;

	private JPanel selectMesure;
	private JPanel infosMesure;
	
	private JComboBox<String> boxDonnees;
	
	private JLabel moment;
	private JLabel rh2S;
	private JLabel rh2C;
	private JLabel pH;
	private JLabel eHS;
	private JLabel eHC;
	private JLabel temp;
	private JLabel conduc;
	private JLabel resis;
	private JLabel humi;
	
	
	public OngletDetailVue (DeviceDataViewerVue laVue, OngletDetailControleur leControleur) {
		this.controleur = leControleur;
		this.vue = laVue;
		
		Vector<String> vec2 = new Vector<String>();
		for(int i = 0;i<vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().size();i++)
			vec2.addElement(Integer.toString(i+1));
		
		boxDonnees = new JComboBox<String>(vec2);
		JLabel Mesure = new JLabel("Mesure N�");
		
		globale = new JPanel();
		
		globale.setLayout(new BorderLayout());
		
		selectMesure = new JPanel();
		
		selectMesure.add(Mesure);
		selectMesure.add(boxDonnees);
		
		globale.add(selectMesure, BorderLayout.NORTH);
		
		infosMesure = new JPanel();
		
		infosMesure.setLayout(new GridLayout(10,3));
		
		JLabel momentT = new JLabel("Date : ");
		JLabel rh2T = new JLabel("rh2 Sonde : ");
		JLabel rh2CT = new JLabel("rh2 Calcul� : ");
		JLabel pHT = new JLabel("pH : ");
		JLabel eHST = new JLabel("eH Sonde : ");
		JLabel eHCT = new JLabel("eH Calcul� : ");
		JLabel tempT = new JLabel("Temp�rature (�C) : ");
		JLabel conducT = new JLabel("\u03C3 : " );
		JLabel resisT = new JLabel("Resistivit� : ");
		JLabel humiT = new JLabel("Humidit� : ");
		
		momentT.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		rh2T.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		rh2CT.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		pHT.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		eHST.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		eHCT.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		tempT.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		conducT.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		resisT.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		humiT.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		
		moment = new JLabel("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getMomentReleve().getMomentString());
		rh2S = new JLabel("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getRh2());
		rh2C = new JLabel("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getRh2Calcul());
		pH = new JLabel("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getpHValue());
		eHS =new JLabel("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getEhSonde());
		eHC = new JLabel("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getEhCalcul());
		temp = new JLabel("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getTemperature());
		conduc =new JLabel("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getConductivite());
		resis = new JLabel("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getResistivite());
		humi = new JLabel("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getHumidite());
		
		moment.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		rh2S.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		rh2C.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		pH.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		eHS.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		eHC.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		temp.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		conduc.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		resis.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		humi.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		
		infosMesure.add(momentT);
		infosMesure.add(moment);
		infosMesure.add(rh2T);
		infosMesure.add(rh2S);
		infosMesure.add(rh2CT);
		infosMesure.add(rh2C);
		infosMesure.add(pHT);
		infosMesure.add(pH);
		infosMesure.add(eHST);
		infosMesure.add(eHS);
		infosMesure.add(eHCT);
		infosMesure.add(eHC);
		infosMesure.add(tempT);
		infosMesure.add(temp);
		infosMesure.add(conducT);
		infosMesure.add(conduc);
		infosMesure.add(resisT);
		infosMesure.add(resis);
		infosMesure.add(humiT);
		infosMesure.add(humi);
		
		boxDonnees.addItemListener(new ItemChangeListener() {
		      public void itemStateChanged(ItemEvent arg0) {
		        controleur.changeContentDetail(vue.getAppareilDansScroll().getSelectedIndex()+1);
		    }
		});
		
		globale.add(infosMesure, BorderLayout.CENTER);
		
		globale.setVisible(true);
	}
	
	public void refreshOngletDetail(int iD) {
		this.moment.setText("" + vue.getModele().getListeAppareils().get(iD).getDonnees().get(this.boxDonnees.getSelectedIndex()).getMomentReleve().getMomentString());
		this.rh2S.setText("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getRh2());
		this.rh2C.setText("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getRh2Calcul());
		this.pH.setText("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getpHValue());
		this.eHS.setText("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getEhSonde());
		this.eHC.setText("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getEhCalcul());
		this.temp.setText("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getTemperature());
		this.conduc.setText("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getConductivite());
		this.resis.setText("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getResistivite());
		this.humi.setText("" + vue.getModele().getListeAppareils().get(vue.getAppareilDansScroll().getSelectedIndex()+1).getDonnees().get(boxDonnees.getSelectedIndex()).getHumidite());
		control();
	}
	
	public JPanel getGlobale() {
		return globale;
	}
	
	public void control() {
		if (Float.valueOf(this.humi.getText()) <35) {
			rh2S.setForeground(Color.red);
			rh2C.setForeground(Color.red);
			pH.setForeground(Color.red);
			eHS.setForeground(Color.red);
			eHC.setForeground(Color.red);
			temp.setForeground(Color.red);
			conduc.setForeground(Color.red);
			resis.setForeground(Color.red);
			humi.setForeground(Color.red);
		}
		else {
			rh2S.setForeground(Color.black);
			rh2C.setForeground(Color.black);
			pH.setForeground(Color.black);
			eHS.setForeground(Color.black);
			eHC.setForeground(Color.black);
			temp.setForeground(Color.black);
			conduc.setForeground(Color.black);
			resis.setForeground(Color.black);
			humi.setForeground(Color.black);
			
		}
	}

}
