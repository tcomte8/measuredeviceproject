package mvc;

public class DeviceDataViewerControleur {
	

	private DeviceDataViewerVue vue = null;
	
	public DeviceDataViewerControleur() {
		
	}
	
	public void addView(DeviceDataViewerVue laVue) {
		this.vue = laVue;
	}
	
	public void changeContentTab(int iD) {
		vue.refreshTab(iD);
		vue.editLabelDesc();
	}
	
	public void openConfigurationTab() {
		vue.openConfigurationTab();
	}

}
