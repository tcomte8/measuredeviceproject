package mvc;

public class FenetreConfigurationControleur {
	
	private DeviceDataViewerModele model;
	private FenetreConfigurationVue vue= null;
	
	public FenetreConfigurationControleur(DeviceDataViewerModele leModel) {
		model = leModel;
	}
	
	public void addView(FenetreConfigurationVue laVue) {
		this.vue = laVue;
	}
	
	public void setDescription(int iD, String desc) {
		this.model.setDescription(iD, desc);
	}
	
	public void editConfFile(int iD, boolean test) {
		vue.editConfFile(iD, test);
	}
	
	public void changeContentTextField() {
		vue.changeContentTextField();
	}

}
