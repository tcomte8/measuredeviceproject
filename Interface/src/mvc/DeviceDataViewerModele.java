package mvc;
import usedClass.Appareil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Observable;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class DeviceDataViewerModele extends Observable{
	
	
	
	private ArrayList<Appareil> listeAppareils;
	//private DeviceDataViewerVue fenetre;

	public DeviceDataViewerModele() {
		// TODO Auto-generated method stub
		listeAppareils = new ArrayList<Appareil>();
		
		chercherDonnees(listeAppareils);
		
		//fenetre = new DeviceDataViewerVue(listeAppareils);
		
		if (listeAppareils != null)
			recupererDesc();

		
	}
	
	public static void chercherDonnees(ArrayList<Appareil> listeAppareils) {
		
		int i = 1;
		
		int exists = 0;
		
		listeAppareils.add(null);
		
		//String cheminAbsolu = "src/datas/";
		String cheminAbsolu = "";
		String intFichier = "001";
		String Extension = ".dat";
		String cheminComplet = "001.dat";
		//String cheminComplet = "src/datas/001.dat";
		
		File dataFile = new File("001.dat");
		//File dataFile = new File("src/datas/001.dat");
		
		while (dataFile.exists()) {
			
			exists = 1;
			
			System.out.println(cheminComplet);
			
			Appareil app = new Appareil(intFichier);
			
			listeAppareils.add(app);
			
			app.EntrerDonnees(cheminComplet);
		
			i++;
			
			if (i > 0) {
				intFichier = "00" + i;
			}
			
			
			if (i > 9) {
				intFichier = "0" + i; 
			}
			
			if (i > 99) {
				intFichier = "" + i;
			}
			
			
			
			cheminComplet = cheminAbsolu + intFichier + Extension;
			
			
			
			dataFile = new File(cheminComplet);			
		}
		
		if (exists == 0 ) {
			JFrame frame = new JFrame("showMessageDialog");
		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    JOptionPane.showMessageDialog(frame, "Veuillez v�rifier que des fichiers du type 'XXX.dat' sont pr�sents dans le m�me r�p�rtoire que l'application", "Fichiers Donn�es Introuvables", JOptionPane.INFORMATION_MESSAGE);
		    System.exit(1);
		    
		}
		
	}
	
	public void recupererDesc() {
		File dataFile = new File("DescApp.conf");
		try {
			
			InputStream ips = new FileInputStream(dataFile);
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			String ligne;
			int j = 1;
			while ((ligne = br.readLine()) != null) {
				
				// recuperation de la ligne courante
				//System.out.println("Contenu de la ligne:" + ligne);
				// separation de la ligne avec le toke ":" (double point)
				// 
				String token = ":";
				StringTokenizer stringTokenizer = new StringTokenizer(ligne, token);
				// Parcours des tokens de la ligne
				
				
				
				int i = 0;
				while (stringTokenizer.hasMoreElements()) {
					String element = (String) stringTokenizer.nextElement();
					//System.out.println("Element : " + element);	
					if(i==1) listeAppareils.get(j).setNom(element);
					i++;
				}
				j++;
			}
			br.close();
		} catch (Exception e) {
		System.out.println(e.toString());
		}
	}

	public void setDescription(int iD, String description) {
		this.listeAppareils.get(iD).setNom(description);
		this.sauverDescriptions();
		setChanged();
		notifyObservers();
	}
	
	public void sauverDescriptions () {
		 PrintWriter pWriter;
			try {
				pWriter = new PrintWriter(new FileWriter("DescApp.conf", false));
				for (int i =1 ; i<listeAppareils.size(); i++) {
					pWriter.print(listeAppareils.get(i).getiD() + ":" + listeAppareils.get(i).getNom() + "\n");
				}
			        pWriter.close() ;
			        
			        
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       
	}

	public ArrayList<Appareil> getListeAppareils() {
		return listeAppareils;
	}
	
	
}