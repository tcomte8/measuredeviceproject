package mvc;

public class OngletDetailControleur {
	
	private OngletDetailVue vue= null;
	
	public OngletDetailControleur() {
	}
	
	public void addView(OngletDetailVue laVue) {
		this.vue = laVue;
	}
	
	public void changeContentDetail(int iD) {
		vue.refreshOngletDetail(iD);
	}
	
	
	
}
