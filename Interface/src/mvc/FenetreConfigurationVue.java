package mvc;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import usedClass.ItemChangeListener;

public class FenetreConfigurationVue {
	
	//private static final long serialVersionUID = 1L;
	private JDialog fenetre;
	private DeviceDataViewerModele modele;
	
	private FenetreConfigurationControleur controleur;
	
	private JTextField textField;
	
	private JComboBox<String> boxAppareil;
	
	public FenetreConfigurationVue(JFrame leOwner, String leTitle, DeviceDataViewerModele leModele, FenetreConfigurationControleur leControleur) {
		
		this.modele = leModele;
		this.controleur = leControleur;
		
		JLabel delaiT = new JLabel("D�lai : ");
		JLabel appareil = new JLabel("Appareil : ");
		
		this.controleur.addView(this);
		
		Vector<String> vecAppareil = new Vector<String>();
		for(int i = 1;i<this.modele.getListeAppareils().size();i++)
			vecAppareil.addElement(modele.getListeAppareils().get(i).getiD());
		
		boxAppareil = new JComboBox<String>(vecAppareil);
		
		boxAppareil.addItemListener(new ItemChangeListener() {
		      public void itemStateChanged(ItemEvent arg0) {
		        controleur.changeContentTextField();
		    }
		});
		
		textField = new JTextField();
		
		textField.getDocument().addDocumentListener(new DocumentListener() {
		    private void updateData() {
		        // mise � jour de l'attribut data
		    	controleur.setDescription(boxAppareil.getSelectedIndex()+1, textField.getText());
		    }
		 
		    @Override
		    public void changedUpdate(DocumentEvent e) {}
		 
		    @Override
		    public void insertUpdate(DocumentEvent e) {
		        // mise a jour quand du texte est ins�r� dans le champs
		        updateData();
		    }
		 
		    @Override
		    public void removeUpdate(DocumentEvent e)  {
		        // mise a jour quand du texte est supprim� dans le champs
		        updateData();
		    }
		 
		});
		
		
		
		JComboBox<String> boxDelai;
		
		JPanel panelImmediat = new JPanel(new GridLayout(2,1));
		
		JCheckBox immediat = new JCheckBox("Mesure Imm�diate");
		JButton immediatSave = new JButton("Enregistrer");
		
		
		
		panelImmediat.add(immediat);
		panelImmediat.add(immediatSave);
		
		Vector<String> vecDelai = new Vector<String>();
		
		vecDelai.addElement("1 heure");
		vecDelai.addElement("3 heures");
		vecDelai.addElement("6 heures");
		vecDelai.addElement("12 heures");
		vecDelai.addElement("24 heures");
		
		boxDelai = new JComboBox<String>(vecDelai);
		
		//delaiT, boxDelai, immediat, immediatSave
		

		immediatSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Test");
				controleur.editConfFile(boxDelai.getSelectedIndex(), immediat.isSelected());
				
				
			}
		});
		
		
		this.fenetre = new JDialog(leOwner, leTitle, true);
		
		//this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		this.fenetre.setSize(new Dimension(400,150));
		//.setResizable(false);
		this.fenetre.setLocationRelativeTo(null);
		
		
		this.fenetre.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		
		gbc.insets = new Insets(0, 2,0,0);
		
		gbc.weightx = 1;
		gbc.weighty = 1;
		
		gbc.gridheight = 1;
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.WEST;
		
		this.fenetre.add(delaiT, gbc);
		
		gbc.insets = new Insets(0, 0,0,0);
		
		gbc.gridx = 1;
		
		this.fenetre.add(boxDelai, gbc);
		
		gbc.gridx = 2;
		gbc.gridheight = 1;
		
		gbc.anchor = GridBagConstraints.CENTER;
		
		
		this.fenetre.add(immediat, gbc);
		
		gbc.gridy = 1;
		
		this.fenetre.add(immediatSave, gbc);
		
		gbc.insets = new Insets(0, 2,0,0);
		
		gbc.anchor = GridBagConstraints.WEST;
		
		
		
		textField.setPreferredSize(new Dimension(250, 25));
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		
		this.fenetre.add(appareil, gbc);
		
		gbc.insets = new Insets(0, 0,0,0);
		
		gbc.gridx = 1;
		
		this.fenetre.add(boxAppareil, gbc);
		
				
		gbc.gridx = 2;
		gbc.weightx = 1;
		
		this.fenetre.add(textField, gbc);
	
		textField.setText(this.modele.getListeAppareils().get(1).getNom());
		
		//this.fenetre.add(descEditor, BorderLayout.CENTER);
		
		
		File dataFile = new File("000.conf");
		try {
			
			InputStream ips = new FileInputStream(dataFile);
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			String ligne;
			while ((ligne = br.readLine()) != null) {
				// recuperation de la ligne courante
				//System.out.println("Contenu de la ligne:" + ligne);
				// separation de la ligne avec le toke ":" (double point)
				// 
				String token = ":";
				StringTokenizer stringTokenizer = new StringTokenizer(ligne, token);
				// Parcours des tokens de la ligne
				int i = 0;
				while (stringTokenizer.hasMoreElements()) {
					String element = (String) stringTokenizer.nextElement();
					//System.out.println("Element : " + element);	
					if(i==0) {
						if (element.equals("1")) boxDelai.setSelectedIndex(0);
						if (element.equals("3")) boxDelai.setSelectedIndex(1);
						if (element.equals("6")) boxDelai.setSelectedIndex(2);
						if (element.equals("12")) boxDelai.setSelectedIndex(3);
						if (element.equals("24")) boxDelai.setSelectedIndex(4);
					}
					if(i==1) if(element.equals("1")) immediat.setSelected(true);
					
					i++;
				}
				
			}
			br.close();
		} catch (Exception e) {
		System.out.println(e.toString());
		}
		
		
		this.fenetre.setResizable(false);
		this.fenetre.setVisible(true);
			
		
	}
	
	public void editConfFile(int iD, boolean test) {
         
        /*Ici j'ai ajout� le param�tre nomm� "append" qui, si plac� � true
        permet d'�crire � la suite du fichier sans effacer d'abord.
        C'est plus pratique quand on veut garder les anciens num�ros  */
        PrintWriter pWriter;
		try {
			pWriter = new PrintWriter(new FileWriter("000.conf", false));
			System.out.println(iD);
			if (iD ==  0) pWriter.print("1:");
		    if (iD ==  1) pWriter.print("3:");
		    if (iD ==  2) pWriter.print("6:");
		    if (iD ==  3) pWriter.print("12:");
		    if (iD ==  4) pWriter.print("24:");
		    if (test) pWriter.print("1:");
		    else pWriter.print("0:");
		        pWriter.close() ;
		        
		        JFrame frame = new JFrame("showMessageDialog");
			    frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			    JOptionPane.showMessageDialog(frame, "Fichier 000.conf �dit�.", "Information", JOptionPane.INFORMATION_MESSAGE);
			    
		        
		        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
		
		
	}
	
	public void changeContentTextField() {
		textField.setText(this.modele.getListeAppareils().get(this.boxAppareil.getSelectedIndex()+1).getNom());
		
		
	}
	
	
	
}
