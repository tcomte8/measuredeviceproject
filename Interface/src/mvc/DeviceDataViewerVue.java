package mvc;
import usedClass.XYPointChart;
import usedClass.XYLineBaseChart;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
 
public class DeviceDataViewerVue implements Observer{
	
	protected DeviceDataViewerModele modele;
	protected DeviceDataViewerControleur controleur;

	//private static final long serialVersionUID = 1L;
	
	

	private JFrame DeviceDataViewerJFrame;
	
	private JList<String> appareilDansScroll;

	private JButton configurer;
	
	private JLabel labID;	    //Label Champs du Titre
	private JLabel labDesc;     //Label Champs Description
	
	private JPanel Detail;
	
	private OngletDetailVue gauche;
	private OngletDetailVue droite;

	private JTabbedPane CardsContainer;
	
	private XYPointChart ChartGraph;
	private XYLineBaseChart ChartpH;
	//private XYLineChart ChartO2;
	private XYLineBaseChart CharteHSonde;
	private XYLineBaseChart ChartTemperature;
	private XYLineBaseChart ChartConductivite;
	private XYLineBaseChart ChartResistivite;
	private XYLineBaseChart ChartHumidite;
	
	
	
 
	public DeviceDataViewerVue(DeviceDataViewerModele leModele, DeviceDataViewerControleur leControleur){

		OngletDetailControleur controleurgauche = new OngletDetailControleur();
		OngletDetailControleur controleurdroit = new OngletDetailControleur();
		
		this.modele = leModele;
		this.controleur = leControleur;
		
		modele.addObserver(this);
		
		DeviceDataViewerJFrame = new JFrame();
		
		//FenetreConfigurationVue configurationVue = new FenetreConfigurationVue();
		
		this.DeviceDataViewerJFrame.setLayout(new BorderLayout());
		this.DeviceDataViewerJFrame.setSize(690,490);
		this.DeviceDataViewerJFrame.setTitle("Measure Data Viewer");
		this.DeviceDataViewerJFrame.setResizable(true);
		this.DeviceDataViewerJFrame.setLocationRelativeTo(null);
		
		//**************
		// la liste WEST
		//**************
		
		DefaultListModel<String> vec = new DefaultListModel<String>();
		for(int i = 1;i<modele.getListeAppareils().size();i++)
			vec.addElement(modele.getListeAppareils().get(i).getiD() + ":" + modele.getListeAppareils().get(i).getNom());

		appareilDansScroll = new JList<String>(vec);
		
		JScrollPane guiListeAppareils = new JScrollPane(appareilDansScroll);
		guiListeAppareils.setPreferredSize(new Dimension(180,490));
		
		configurer = new JButton("Configurer");  //Bouton Configurer
		
		configurer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controleur.openConfigurationTab();
				
			}
		});
		
		JPanel guiGlobalWest = new JPanel();
		guiGlobalWest.setLayout(new BorderLayout());
		guiGlobalWest.add(guiListeAppareils, BorderLayout.WEST);
		guiGlobalWest.add(configurer, BorderLayout.SOUTH);
        
		//*********************************
		// les informations disponible EAST
		//*********************************
		
		// les saisies Utilisateur
		JPanel guiDataAppareils = new JPanel();
		guiDataAppareils.setLayout(new GridBagLayout());
		guiDataAppareils.setPreferredSize(new Dimension(550, 490));
		GridBagConstraints gbc = new GridBagConstraints();
		
		JLabel labIDT = new JLabel("ID : ");      //Label Titre
		JLabel labDescT = new JLabel("Description : ");    //Label Description
		labID = new JLabel(modele.getListeAppareils().get(1).getiD());	    //Label Champs du Titre
		labDesc = new JLabel(modele.getListeAppareils().get(1).getNom());     //Label Champs Description
		
		
		//Case de d�part du composant
		gbc.gridx = 0;
		gbc.gridy = 0;		
		
		gbc.weightx = 0.1;
		gbc.weighty = 0.05;
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.LINE_START;
		
		//Taille du composant
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		
		guiDataAppareils.add(labIDT,gbc);
		
		gbc.gridx = 1;
		gbc.weightx = 1;
		
		guiDataAppareils.add(labID, gbc);
		
	
		gbc.gridx = 3;
		gbc.gridwidth = 1;
		
		guiDataAppareils.add(labDescT, gbc);
		
		
		gbc.gridx = 4;
		gbc.gridwidth = 8;
		
		guiDataAppareils.add(labDesc, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 15;
		gbc.gridheight = 8;
		gbc.gridwidth = GridBagConstraints.LINE_START;
		
		CardsContainer = new JTabbedPane();
		CardsContainer.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 0.9;
		
		Detail = new JPanel();
		
		
		
		
		CardsContainer.addTab(" D�tails ", null, Detail, "Mesures de l'appareil");
		CardsContainer.setMnemonicAt(0,  KeyEvent.VK_1);
		guiDataAppareils.add(CardsContainer, gbc);
		
		ChartGraph = new XYPointChart();
		CardsContainer.addTab(" rh2/pH ", null, ChartGraph.createChartPanel("rh2 en fonction du pH", "pH", "rh2"), "Graphique rh2=f(pH)");
		CardsContainer.setMnemonicAt(1,  KeyEvent.VK_2);
		guiDataAppareils.add(CardsContainer, gbc);
		
		ChartpH = new XYLineBaseChart();
		CardsContainer.addTab("   pH   ", null, ChartpH.createChartPanel("pH", "Num�ro Mesure", "Value"), "Graphique pH=f(t)");
		CardsContainer.setMnemonicAt(2,  KeyEvent.VK_3);
		guiDataAppareils.add(CardsContainer, gbc);
		
		/*
		ChartO2 = new XYLineChart();
		CardsContainer.addTab("[O2]", null, ChartO2.createChartPanel("[O2]", "Num�ro Mesure", "Value"), "Graphique [O2]=f(t)");
		CardsContainer.setMnemonicAt(3,  KeyEvent.VK_4);
		guiDataAppareils.add(CardsContainer, gbc);
		*/
		
		CharteHSonde = new XYLineBaseChart();
		CardsContainer.addTab("   eH   ", null, CharteHSonde.createChartPanel("eH", "Num�ro Mesure", "Value"), "Graphique Eh=f(t)");
		CardsContainer.setMnemonicAt(3,  KeyEvent.VK_4);
		guiDataAppareils.add(CardsContainer, gbc);
		
		ChartTemperature = new XYLineBaseChart();
		CardsContainer.addTab("   T�   ", null, ChartTemperature.createChartPanel("Temp�rature", "Num�ro Meesure", "Value"), "Graphique T=f(t)");
		CardsContainer.setMnemonicAt(4,  KeyEvent.VK_5);
		guiDataAppareils.add(CardsContainer, gbc);
		
		ChartConductivite = new XYLineBaseChart();
		CardsContainer.addTab("   \u03C3   ", null, ChartConductivite.createChartPanel("Conductivit�", "Num�ro Mesure", "Value"), "Graphique Conductivite=f(t)");
		CardsContainer.setMnemonicAt(5,  KeyEvent.VK_6);
		guiDataAppareils.add(CardsContainer, gbc);
		
		ChartResistivite = new XYLineBaseChart();
		CardsContainer.addTab("   R   ", null, ChartResistivite.createChartPanel("R�sistivit�", "Num�ro Mesure", "Value"), "Graphique Resistivit�=f(t)");
		CardsContainer.setMnemonicAt(6,  KeyEvent.VK_7);
		guiDataAppareils.add(CardsContainer, gbc);
		
		ChartHumidite = new XYLineBaseChart();
		CardsContainer.addTab("  H(%)  ", null, ChartHumidite.createChartPanel("Humidit� (%)", "Num�ro Mesure", "Value"), "Graphique Humidite=f(t)");
		CardsContainer.setMnemonicAt(7,  KeyEvent.VK_8);
		guiDataAppareils.add(CardsContainer, gbc);
		
		ValueMarker marker = new ValueMarker(35);  
		marker.setPaint(Color.black);
		XYPlot plot = (XYPlot) ChartHumidite.getChart().getPlot();
		plot.addRangeMarker(marker);
		
		appareilDansScroll.setSelectedIndex(0);
		
		refreshTab(appareilDansScroll.getSelectedIndex()+1);
		
		OngletDetailVue gauche = new OngletDetailVue(this, controleurgauche);
		OngletDetailVue droite = new OngletDetailVue(this, controleurdroit);
		
		controleurgauche.addView(gauche);
		controleurdroit.addView(droite);
		
		Detail.setLayout(new GridLayout(1,2));
		
		Detail.add(gauche.getGlobale());
		
		appareilDansScroll.addListSelectionListener(new ListSelectionListener() {
		      public void valueChanged(ListSelectionEvent le) {
		    	  if(appareilDansScroll.getSelectedIndex() != -1) {
		        controleur.changeContentTab(appareilDansScroll.getSelectedIndex()+1);
		        controleurdroit.changeContentDetail(appareilDansScroll.getSelectedIndex()+1);
		        controleurgauche.changeContentDetail(appareilDansScroll.getSelectedIndex()+1);
		    	  }
		    }
		});
		
		JSplitPane splitPaneDetail = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,null,droite.getGlobale());
		splitPaneDetail.setOneTouchExpandable(false);
		splitPaneDetail.setDividerSize(10);
		splitPaneDetail.setBackground(Color.lightGray);
		splitPaneDetail.setResizeWeight(0);
		
		Detail.add(splitPaneDetail);
		//Detail.add(droite.getGlobale());
 
		// on fait un split pour le jolie
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,null,guiDataAppareils);
		splitPane.setOneTouchExpandable(false);
		splitPane.setDividerSize(10);
		splitPane.setBackground(Color.lightGray);
		splitPane.setResizeWeight(0.8);
 
		
		
		// on ajoute le tout dans la fen�tre
		this.DeviceDataViewerJFrame.add(guiGlobalWest,BorderLayout.WEST);
		this.DeviceDataViewerJFrame.add(splitPane,BorderLayout.CENTER);
 
		//On quitte l'application quand la fen�tre est ferm�e
		this.DeviceDataViewerJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.DeviceDataViewerJFrame.setVisible(true);
		
		//modele.getListeAppareils().get(1).setNom("Champs 52");
		//update(null, null);
	}
	
	public DeviceDataViewerModele getModele() {
		return modele;
	}
	
	public DeviceDataViewerControleur getControleur() {
		return controleur;
	}
	
	public JPanel getDetail() {
		return Detail;
	}

	public JList<String> getAppareilDansScroll() {
		return appareilDansScroll;
	}
	
	public OngletDetailVue getGauche() {
		return gauche;
	}

	public OngletDetailVue getDroite() {
		return droite;
	}

	
	public void refreshTab(int iD) {

		ChartpH.refreshChart(modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllpH(), "pH", modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllMoment());
		ChartGraph.refreshChart(modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllpH(), "rh2 Mesur�", modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllRh2(),modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllpH(), "rh2 Calcul�", modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllRh2Calcul());
		//ChartO2.refreshChart(modele.getListeAppareils().get(iD).getAllConcentrationO2(), "[O2]");		
		CharteHSonde.refreshChart(modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAlleHSonde(), "eH Sonde",modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllMoment(), modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAlleHCalcul(), "eH Calcul", modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllMoment());
		ChartTemperature.refreshChart(modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllTemperature(), "T�", modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllMoment());
		ChartConductivite.refreshChart(modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAlConductivite(), "\u03C3", modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllMoment());
		ChartResistivite.refreshChart(modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllResistivite(), "R", modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllMoment());
		ChartHumidite.refreshChart(modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllHumidite(), "H", modele.getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getAllMoment());
		
				
	}
	
	public void openConfigurationTab() {
		FenetreConfigurationControleur leControleur = new FenetreConfigurationControleur(modele);
		new FenetreConfigurationVue(DeviceDataViewerJFrame, "Configuration", modele, leControleur);
	}
	
	public void editLabelDesc() {
		this.labDesc.setText(this.getModele().getListeAppareils().get(this.appareilDansScroll.getSelectedIndex()+1).getNom());
	}
	
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
		// On m�morise le champs s�lectionn� 
		int id = appareilDansScroll.getSelectedIndex();
		
		// On actualise La JListe
		Vector<String> vec = new Vector<String>();
		for(int i = 1;i<modele.getListeAppareils().size();i++)
			vec.add( modele.getListeAppareils().get(i).getiD() + ":" + modele.getListeAppareils().get(i).getNom());
		
		appareilDansScroll.setListData(vec);
		
		// On reselectionne le bon champs
		appareilDansScroll.setSelectedIndex(id);
		/*
		//On actualise l'affichage
		labID.setText(modele.getListeAppareils().get(id+1).getiD());
		labDesc.setText(modele.getListeAppareils().get(id+1).getNom());
		*/
		
		
	}
	
	
 
}