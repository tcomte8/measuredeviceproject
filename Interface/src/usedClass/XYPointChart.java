package usedClass;

import java.util.ArrayList;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class XYPointChart {

	private XYSeriesCollection data;
	
	public XYPointChart() {
		this.data = new XYSeriesCollection();
	}
	
	
	
	public void AddDataSet(ArrayList <Float> abscisse, String nom, ArrayList<Float> ordonnee) {
		
		XYSeries serie = new XYSeries(nom, false);
		
		//System.out.println(donnees);
		
		for (int i = 0; i<abscisse.size(); i++) 
			serie.add(abscisse.get(i), ordonnee.get(i));
		
		this.data.addSeries(serie);
		
	}
	
	public JPanel createChartPanel(String nomPanel, String nomXAxis, String nomYAxis) {
		
		JFreeChart chart =  ChartFactory.createScatterPlot(nomPanel, nomXAxis, nomYAxis, this.data);
	
		return new ChartPanel(chart);
	}
	
	
	
	public void RemoveAllData() {
		this.data.removeAllSeries();
	}
	
	
	public void refreshChart(ArrayList <Float> abscisse1, String nom1, ArrayList <Float> ordonnee1, ArrayList <Float> abscisse2, String nom2, ArrayList <Float> ordonnee2) {
		this.RemoveAllData();
		this.AddDataSet(abscisse1, nom1, ordonnee1);
		this.AddDataSet(abscisse2, nom2, ordonnee2);
		//this.createChartPanel(nomPanel, nomXAxis, nomYAxis);
	}

}
