package usedClass;

public class Moment {
	private int heure;
	private int jour;
	private int jourSemaine;
	private int mois;
	private int annee;
	
	public Moment () {
	}
	
	public int getHeure() {
		return heure;
	}
	public void setHeure(int heure) {
		this.heure = heure;
	}
	public int getJour() {
		return jour;
	}
	public void setJour(int jour) {
		this.jour = jour;
	}
	public int getJourSemaine() {
		return jourSemaine;
	}
	public void setJourSemaine(int jourSemaine) {
		this.jourSemaine = jourSemaine;
	}
	public int getMois() {
		return mois;
	}
	public void setMois(int mois) {
		this.mois = mois;
	}
	public int getAnnee() {
		return annee;
	}
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	
	public String getMomentString() {
		return this.jour + "-" + this.mois + "-" + (this.annee + 2000) + " " + this.heure + ":00" ;
	}
}
