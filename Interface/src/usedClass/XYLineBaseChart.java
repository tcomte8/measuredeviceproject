package usedClass;
import java.util.ArrayList;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class XYLineBaseChart {
	private XYSeriesCollection data;
	private ChartPanel panel;
	private JFreeChart chart;
	public XYLineBaseChart() {
		this.data = new XYSeriesCollection();
	}
	
	public void AddDataSet(ArrayList <Float> donnees, String nom, ArrayList <Moment> moment) {
		       
	   
		
		XYSeries serie = new XYSeries(nom, false);

		for (int i = 0; i<donnees.size(); i++) 
			serie.add(i+1, donnees.get(i));
		
		this.data.addSeries(serie);
		
	}
	
	public JPanel createChartPanel(String nomPanel, String nomXAxis, String nomYAxis) {
		chart =  ChartFactory.createXYLineChart(nomPanel, nomXAxis, nomYAxis, this.data);
		panel = new ChartPanel(chart);
		return panel;
	}
	
	
	
	public JFreeChart getChart() {
		return chart;
	}

	public ChartPanel getPanel() {
		return panel;
	}

	public void RemoveAllData() {
		this.data.removeAllSeries();
	}
	
	public void refreshChart(ArrayList <Float> donnees, String nom, ArrayList <Moment> moment) {
		this.RemoveAllData();
		this.AddDataSet(donnees, nom, moment);
		//this.createChartPanel(nomPanel, nomXAxis, nomYAxis);
	}
	
	public void refreshChart(ArrayList <Float> donnees1, String nom1, ArrayList <Moment> moment1, ArrayList <Float> donnees2, String nom2, ArrayList <Moment> moment2) {
		this.RemoveAllData();
		this.AddDataSet(donnees1, nom1, moment1);
		this.AddDataSet(donnees2, nom2, moment2);
		//this.createChartPanel(nomPanel, nomXAxis, nomYAxis);
	}
	

}
