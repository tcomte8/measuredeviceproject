package usedClass;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Appareil {
	private String iD;
	private String nom;
	private ArrayList <Donnees> donnees;



	public Appareil(String ID) {
		this.iD = ID;
		this.nom = "";
	}
	
	public void EntrerDonnees(String nomFichier) {
		this.donnees = new ArrayList<Donnees>();
		//System.out.println(nomFichier);
		File dataFile = new File(nomFichier);
		try {
			
			InputStream ips = new FileInputStream(dataFile);
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			String ligne;
			while ((ligne = br.readLine()) != null) {
				Donnees ligneDonnees = new Donnees();
				// recuperation de la ligne courante
				//System.out.println("Contenu de la ligne:" + ligne);
				// separation de la ligne avec le toke ":" (double point)
				// 
				String token = ":";
				StringTokenizer stringTokenizer = new StringTokenizer(ligne, token);
				// Parcours des tokens de la ligne
				int i = 0;
				while (stringTokenizer.hasMoreElements()) {
					String element = (String) stringTokenizer.nextElement();
					//System.out.println("Element : " + element);	
					if(i==0) ligneDonnees.setpHValue(Float.valueOf(element));
					//if(i==1) ligneDonnees.setConcentrationO2(Float.valueOf(element));
					if(i==2) ligneDonnees.setEhSonde(Float.valueOf(element));
					if(i==3) ligneDonnees.setEhCalcul(Float.valueOf(element));
					if(i==4) ligneDonnees.setTemperature(Float.valueOf(element));
					if(i==5) ligneDonnees.setConductivite(Float.valueOf(element));
					if(i==6) ligneDonnees.setResistivite(Float.valueOf(element));
					if(i==7) ligneDonnees.setHumidite(Float.valueOf(element));
					if(i==8) ligneDonnees.setRh2(Float.valueOf(element));
					if(i==9) ligneDonnees.setRh2Calcul(Float.valueOf(element));
					if(i==10) ligneDonnees.getMomentReleve().setHeure(Integer.valueOf(element));
					if(i==11) ligneDonnees.getMomentReleve().setJour(Integer.valueOf(element));
					if(i==12) ligneDonnees.getMomentReleve().setJourSemaine(Integer.valueOf(element));
					if(i==13) ligneDonnees.getMomentReleve().setMois(Integer.valueOf(element));
					if(i==14) ligneDonnees.getMomentReleve().setAnnee(Integer.valueOf(element));
					i++;
				}
				this.donnees.add(ligneDonnees);
			}
			br.close();
		} catch (Exception e) {
		System.out.println(e.toString());
		}
	}
	
	

	public String getiD() {
		return iD;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList<Donnees> getDonnees() {
		return donnees;
	}
		
	public ArrayList<Float> getAllpH() {
		ArrayList <Float> value = new ArrayList<Float>();
		for (int i = 0; i< this.donnees.size(); i++) {
			value.add(this.donnees.get(i).getpHValue());
		}
		System.out.println(value);
		return value;
	}
	
	/*
	public ArrayList<Float> getAllConcentrationO2() {
		ArrayList <Float> value = new ArrayList<Float>();
		for (int i = 0; i< this.donnees.size(); i++) {
			value.add(this.donnees.get(i).getConcentrationO2());
		}
		System.out.println(value);
		return value;
	}
	*/
	
	public ArrayList<Float> getAlleHSonde() {
		ArrayList <Float> value = new ArrayList<Float>();
		for (int i = 0; i< this.donnees.size(); i++) {
			value.add(this.donnees.get(i).getEhSonde());
		}
		System.out.println(value);
		return value;
	}
	
	public ArrayList<Float> getAlleHCalcul() {
		ArrayList <Float> value = new ArrayList<Float>();
		for (int i = 0; i< this.donnees.size(); i++) {
			value.add(this.donnees.get(i).getEhCalcul());
		}
		System.out.println(value);
		return value;
	}
	
	public ArrayList<Float> getAllTemperature() {
		ArrayList <Float> value = new ArrayList<Float>();
		for (int i = 0; i< this.donnees.size(); i++) {
			value.add(this.donnees.get(i).getTemperature());
		}
		System.out.println(value);
		return value;
	}
	
	public ArrayList<Float> getAlConductivite() {
		ArrayList <Float> value = new ArrayList<Float>();
		for (int i = 0; i< this.donnees.size(); i++) {
			value.add(this.donnees.get(i).getConductivite());
		}
		System.out.println(value);
		return value;
	}
	
	public ArrayList<Float> getAllResistivite() {
		ArrayList <Float> value = new ArrayList<Float>();
		for (int i = 0; i< this.donnees.size(); i++) {
			value.add(this.donnees.get(i).getResistivite());
		}
		System.out.println(value);
		return value;
	}
	
	public ArrayList<Float> getAllHumidite() {
		ArrayList <Float> value = new ArrayList<Float>();
		for (int i = 0; i< this.donnees.size(); i++) {
			value.add(this.donnees.get(i).getHumidite());
		}
		System.out.println(value);
		return value;
	}
	
	public ArrayList<Float> getAllRh2() {
		ArrayList <Float> value = new ArrayList<Float>();
		for (int i = 0; i< this.donnees.size(); i++) {
			value.add(this.donnees.get(i).getRh2());
		}
		System.out.println(value);
		return value;
	}
	
	public ArrayList<Float> getAllRh2Calcul() {
		ArrayList <Float> value = new ArrayList<Float>();
		for (int i = 0; i< this.donnees.size(); i++) {
			value.add(this.donnees.get(i).getRh2Calcul());
		}
		System.out.println(value);
		return value;
	}
	
	public ArrayList<Moment> getAllMoment() {
		ArrayList <Moment> value = new ArrayList<Moment>();
		for (int i = 0; i< this.donnees.size(); i++) {
			value.add(this.donnees.get(i).getMomentReleve());
		}
		System.out.println(value);
		return value;
	}

}