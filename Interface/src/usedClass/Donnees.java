package usedClass;

public class Donnees {
	private float pHValue;
	//private float concentrationO2;
	private float ehSonde;
	private float ehCalcul;
	private float temperature;
	private float conductivite;
	private float resistivite;
	private float humidite;
	private float rh2;
	private float rh2Calcul;
	

	private Moment momentReleve;


	public Donnees() {
		momentReleve = new Moment();
	}


	public float getpHValue() {
		return pHValue;
	}


	public void setpHValue(float pHValue) {
		this.pHValue = pHValue;
	}

	/*
	public float getConcentrationO2() {
		return concentrationO2;
	}


	public void setConcentrationO2(float concentrationO2) {
		this.concentrationO2 = concentrationO2;
	}
	*/

	public float getEhSonde() {
		return ehSonde;
	}


	public void setEhSonde(float ehSonde) {
		this.ehSonde = ehSonde;
	}


	public float getTemperature() {
		return temperature;
	}


	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}


	public float getConductivite() {
		return conductivite;
	}


	public void setConductivite(float conductivite) {
		this.conductivite = conductivite;
	}


	public float getResistivite() {
		return resistivite;
	}


	public void setResistivite(float resistivite) {
		this.resistivite = resistivite;
	}


	public float getHumidite() {
		return humidite;
	}


	public void setHumidite(float humidite) {
		this.humidite = humidite;
	}


	public float getEhCalcul() {
		return ehCalcul;
	}


	public void setEhCalcul(float ehCalcul) {
		this.ehCalcul = ehCalcul;
	}
	
	
	public float getRh2() {
		return rh2;
	}


	public void setRh2(float ehCalcul) {
		this.rh2 = ehCalcul;
	}
	
	public void setMomentReleve(int heure, int jour, int jourSemaine, int mois, int annee) {
		this.momentReleve.setHeure(heure);
		this.momentReleve.setAnnee(annee);
		this.momentReleve.setJour(jour);
		this.momentReleve.setJourSemaine(jourSemaine);
		this.momentReleve.setMois(mois);
	}
	
	public Moment getMomentReleve() {
		return momentReleve;
	}
	
	public float getRh2Calcul() {
		return rh2Calcul;
	}


	public void setRh2Calcul(float rh2Calcul) {
		this.rh2Calcul = rh2Calcul;
	}
}