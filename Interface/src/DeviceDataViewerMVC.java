import mvc.DeviceDataViewerControleur;
import mvc.DeviceDataViewerModele;
import mvc.DeviceDataViewerVue;

public class DeviceDataViewerMVC {
	
	public DeviceDataViewerMVC() {
		DeviceDataViewerModele application = new DeviceDataViewerModele();
		DeviceDataViewerControleur applicationC = new DeviceDataViewerControleur();
		DeviceDataViewerVue applicationV = new DeviceDataViewerVue(application, applicationC);
		applicationC.addView(applicationV);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new DeviceDataViewerMVC();
			}
		});
		
	}

}
