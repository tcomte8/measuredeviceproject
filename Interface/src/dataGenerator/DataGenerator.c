#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

double frand_a_b(double a, double b);
void RandMesures(FILE* fichier, int heure);
void RandMesures(FILE* fichier, int heure) {

	double ph = frand_a_b(0,14);
	double eH = frand_a_b(-2,2);
	double O2 = frand_a_b(15,25);
	double eHCalcul = frand_a_b(-2,2);
	double T = frand_a_b(-15, 50);
	double Conduc = frand_a_b(1000,2000);
	double Resis = 1.0/Conduc;
	double H = frand_a_b(0,100);
	double rh2 = frand_a_b(0,42);
	double rh2Calcul = frand_a_b(0,42);

	//printf("%f:%f:%f:%f:%f:%f:%f:%f\n", ph, eH, O2, eHCalcul, T, Conduc, Resis, H);
	fprintf(fichier, "%f:%f:%f:%f:%f:%f:%f:%f:%f:%f:%d:21:4:3:19\n", ph, O2, eH, eHCalcul, T, Conduc, Resis, H, rh2, rh2Calcul, heure);

}

double frand_a_b(double a, double b) {
	return ( rand()/(double)RAND_MAX)*(b-a)+a;
}

int main (void) {

	int nombreAppareils;
	int nombreMesures;
	int i, j;
	
	char extention[5] = ".dat";

	FILE *fichier = NULL;

	printf("Nombre d'appareils : ");
	scanf("%d", &nombreAppareils);
	printf("Nombre de Mesures : ");
	scanf("%d", &nombreMesures);

	for (i=0; i<nombreAppareils; i++) {

		char nomFichier[8];

		if (i+1<100) {
			sprintf(nomFichier, "0%d", i+1);
		}

		if (i+1<10) {
			sprintf(nomFichier, "00%d", i+1);
		}

		strcat(nomFichier, extention);

		fichier = fopen(nomFichier, "w");

		for (j=0; j<nombreMesures; j++) {
			RandMesures(fichier, j+1);

		}

		fclose(fichier);
	}

}